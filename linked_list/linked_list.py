# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_list.
#
# Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_19.py.
class LinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
        self.length = 0

    def insert(self, value, idx=None):
        node = LinkedListNode(value)
        if self.head is None:
            self.head = node
            self.tail = node

        elif idx is None and self.head is not None:
            self.tail.link = node
            self.tail = node
        else:
            pass
        self.length += 1

    def get(self, index):
        if index >= self.length:
            raise IndexError("index out of range")
        else:
            node = self.head
            for idx in range(self.length):
                if idx == index:
                    return node.value
                else:
                    temp = node
                    node = temp.link
        return None

class LinkedListNode:
    def __init__(self, value, link=None):
        self.value = value
        # link will be linked queue node object
        self.link = link
