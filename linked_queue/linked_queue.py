# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_queue.
#
# # Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_15.py.
class LinkedQueue:
    def __init__(self):
        self.head = None
        self.tail = None

    def dequeue(self):
        # remove linked queue node from Linked Que
        value = self.head.value
        if self.tail is not None:
            if self.tail != self.head:
                value = self.head.value
                queue_node = self.head.link
                self.head = queue_node
            else:
                queue_node = self.head
                self.head = None
                self.tail = None
        return value


    def enqueue(self, value):
        # add linked queue node to Linked Queue
        # value should be LinkedQueueNode to attach to tail.link
        queue_node = LinkedQueueNode(value)
        if self.head is None and value is not None:
            self.head = queue_node
            self.tail = queue_node
        elif self.head == self.tail:
            self.head.link = queue_node
            self.tail = queue_node
            queue_node = self.tail
        else:
            self.tail.link = queue_node
            self.tail = queue_node


class LinkedQueueNode:
    def __init__(self, value, link=None):
        self.value = value
        # link will be linked queue node object
        self.link = link
